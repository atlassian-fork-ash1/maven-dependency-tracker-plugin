package com.atlassian.maven.plugins

/**
 * @requiresDependencyResolution test
 * @goal validate
 * @phase validate
 */
class ValidateDependenciesMojo extends AbstractDependencyTrackerMojo
{
    /**
     *
     * @parameter expression="${dependency-tracker.skip}" default-value="false"
     * @required
     * @readonly
     */
    boolean skip = false

    /**
     *
     * @parameter expression="${dependency-tracker.validateArtifactChecksums}" default-value="true"
     * @required
     */
    boolean validateArtifactChecksums

    /**
     *
     * @parameter expression="${dependency-tracker.validatePomChecksums}" default-value="true"
     * @required
     */
    boolean validatePomChecksums

    /**
     * @parameter expression="${dependency-tracker.failMessage}"
     */
    String failMessage

    void execute()
    {
        if (skip)
        {
            log.info "Skipping dependency report validation"
            return
        }

        if (diffReports(validateArtifactChecksums, validatePomChecksums)) {
            if (failMessage) {
                log.error failMessage
            }
            fail "The dependency report and your resolved dependencies are not in sync. An updated report has been written to <${actualReportDir.absolutePath}>"
        }
    }
}
